local ReplicatedStorage = game:GetService("ReplicatedStorage")
local Packages = ReplicatedStorage.Packages

local Components = require(Packages.Components)

return function(target)
    local component = Components.HelloWorld({
        Parent = target
    })

    return function()
        component:Destroy()
    end
end