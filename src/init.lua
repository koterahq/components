type component = ({ any }) -> (Instance)
export type components = {
    _version: string,
    HelloWorld: component
}

local HelloWorld = require(script.HelloWorld)

return {
    _version = "0.1.0",
    HelloWorld = HelloWorld
} :: components