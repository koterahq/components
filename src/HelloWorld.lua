local Components = script.Parent
local Packages = script.Parent.Parent
local Fusion = require(Packages.Fusion)

local New = Fusion.New
local State = Fusion.State
local OnEvent = Fusion.OnEvent

local HOVER_STATE = "Hello, world!"
local DEFAULT_STATE = "Hey!! Hover at me!"

return function(props)
    local TextState = State(DEFAULT_STATE)
    return New "TextLabel" {
        AnchorPoint = Vector2.new(0.5, 0.5),
        Size = UDim2.fromOffset(200, 50),
        Position = UDim2.fromScale(0.5, 0.5),
        Text = TextState,
        Parent = props.Parent,

        [OnEvent "MouseEnter"] = function()
            TextState:set(HOVER_STATE)
        end,

        [OnEvent "MouseLeave"] = function()
            TextState:set(DEFAULT_STATE)
        end
    }
end